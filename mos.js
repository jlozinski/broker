var mosca = require('mosca')
var fs = require('fs')
/*var readline = require('readline')

  var rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
  });*/

/*rl.question("Input: \'topic`payload\'", function(answer) => 
  {
  var szi = answer.split("`");
  var msg =
  {	
  topic: szi[0],
  payload: szi[1],
  qos: 1,
  retain: false
  };
  server.publish(msg);
  rl.close()
  });*/


var pubsubSettings = 
{
type: 'mqtt',
      json: false,
      mqtt: require('mqtt')
};

var moscaSettings = 
{
    port: 1883
};

var server = new mosca.Server(moscaSettings);
server.on('ready', setup)
var stream = fs.createWriteStream("con.log", {flags:'a'});

function setup()
{
    console.log('Mosca server is up and running')
}

var message = 
{
    topic: '/all/',
    payload: 'You\'re connected',
    qos: 1,
    retain: false
};

server.on('clientConnected', (client) => 
{
    console.log('Client connected: ', client.id);
    //server.publish(message);
});

server.on('clientDisconnected', (client) => 
{
    console.log('Client disconnected: ', client.id);
});

server.on('published', (packet, client) => 
{
    if(packet.topic.substring(0, 5) !== "$SYS/")
    {
	console.log("Publish["+packet.topic+"]: " + packet.payload.toString("hex"));
    }
});
