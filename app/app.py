from flask import Flask, request, render_template
import paho.mqtt.client as mqtt
from datetime import datetime
import sqlite3 as sql
app = Flask(__name__)

#database
conn = sql.connect('database.db')
print ("Opened database successfully")
#conn.execute('DROP TABLE IF EXISTS logs')
conn.execute('CREATE TABLE IF NOT EXISTS logs (dateTime TEXT, topic TEXT, payload TEXT, deviceType TEXT, onOff TEXT)')
print ("Table created successfully");
conn.close()

@app.route('/', methods=['GET', 'POST'])
def homepage():    
    # this block is only entered when the form is posted
    if request.method == 'POST':  
        dateTime = datetime.now().strftime('%Y-%m-%d %H:%M:%S')    
        # data from form input #G/00FF200025/011/0000 #6aa07d00f002810280000100
        topic = "G/"+request.form.get('serialNum')+"/" +request.form.get('msgType')+"/"+request.form.get('attId')
        serial = request.form.get('deviceSerialNum')
        deviceType = serial[0:2]        
        onOff = request.form.get('onOff')  
        
        actuate_code = ""
        if deviceType.lower() == "6a":
            #this is light
            deviceType = "Light"
            actuate_code = "0081028000"
        else:
            #this is gdo
            deviceType = "Gate"
            actuate_code = "0080020001"	
            
        # translate to mqtt
        if onOff is None:
            onOff = ''
        else:
            if onOff == "On":
                onOff = "0100"
            elif onOff == "Off":
                onOff = "0000"        
        
        # new_serial = deviceType + serial[len(serial)-1] + "0"
        new_serial = serial[0:2] + serial[8] + "0" + serial[6:8] + serial[4:6] + serial[2:4]

        payload = new_serial + actuate_code + onOff
        
        # connect to mqtt broker server
        connectClient(topic, payload)
        
        # insert to db   
        con = sql.connect("database.db")
        cur = con.cursor()                
        cur.execute("INSERT INTO logs (dateTime, topic, payload, deviceType, onOff) VALUES (?,?,?,?,?)",(dateTime, topic, payload, deviceType, request.form.get('onOff')))                
        con.commit()
        msg = "Activity log successfully added"
        
        
		#con = sql.connect("database.db")
        con.row_factory = sql.Row   
        cur = con.cursor()
        cur.execute("select * from logs")
        rows = cur.fetchall();
        con.close()
        return render_template('home.html',rows = rows) 
    # initial page form
    con = sql.connect("database.db")
    con.row_factory = sql.Row   
    cur = con.cursor()
    cur.execute("select * from logs")
    rows = cur.fetchall();
    return render_template('home.html',rows = rows) 

# def fetchList():
#     con = sql.connect("database.db")
#     con.row_factory = sql.Row   
#     cur = con.cursor()
#     cur.execute("select * from logs")
#     rows = cur.fetchall();
#     return render_template('home.html',rows = rows)   
    
def connectClient(topic, payload):
    
    client = mqtt.Client()

    client.connect_async('127.0.0.1', 1883, 60)
    client.loop_start()
    client.publish(topic, payload=bytearray.fromhex(payload), qos=1)
    #client.publish(topic, payload, qos=1)
    #print(topic + ":" + payload)

if __name__ == '__main__':
    app.run("0.0.0.0",port=5000,debug=True)
